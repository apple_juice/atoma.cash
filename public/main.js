"use strict";

window.addEventListener('DOMContentLoaded', function () {
  function isMobile() {
    return (/Android|iPhone|iPad|iPod|BlackBerry/i).test(navigator.userAgent || navigator.vendor || window.opera);
  }

  if (isMobile()) {
    document.body.classList.add('body--mobile');
  } else {
    document.body.classList.remove('body--mobile');
  }

  const elems = document.querySelectorAll('.image_tilt');
  const tilt = UniversalTilt.init({
    elements: elems,
    settings: {
      max: 10,
      speed: 800,
      reverse: true,
      base: "window"
    }
  });
});